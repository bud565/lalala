<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Registrasi</title>	
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="shortcut icon" href="flat.ico">
	<script src="bootstrap/js/jquery.min.js"></script>
	<script src="bootstrap/js/popper.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<style>
		h2{
			font-family:"Rockwell";
			text-align: center;
			color:Gray;
	line-height: 30px;
	letter-spacing: 5px;
		}
			h3,p{
			color:white;
		}
	</style>
</head>
<script> document.onkeydown = function (event) { switch (event.key) { 
case "F2": setTimeout('self.location.href="admin/index.php"', 0); break;
case "F5": setTimeout('window.location.reload();', 0); break; } 
 }; </script>
<body style="background-color:#343a40;">
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<ul class="navbar-nav">
			<li class="nav-item active">
				<a class="nav-link">HOME</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="pdo_show.php">LIST</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="data/publish.php">PUBLISH</a>
			</li>
			<?php
			
			if(isset($_SESSION["nim"]))
			{
				
				echo '<li class="nav-item">';
					echo '<a class="nav-link" style="margin-left:950px;">'.$_SESSION["nim"].'</a>';
				echo '</li>';
				echo '<li class="nav-item">';
					echo '<a class="nav-link" href="logout.php">Logout</a>';
				echo '</li>';
			}
			else  
			{  
				echo '<li class="nav-item">';
					echo "<a class='nav-link' href='pdo_login.php' style='margin-left:1050px;'>LOGIN</a>";
				echo '</li>';
			}  
			?>  
		</ul>
	</nav>
	
	<div id="demo" class="carousel slide" data-ride="carousel">

		<!-- Indicators -->
		<ul class="carousel-indicators">
			<li data-target="#demo" data-slide-to="0" class="active"></li>
			<li data-target="#demo" data-slide-to="1"></li>
			<li data-target="#demo" data-slide-to="2"></li>
		</ul>

		<!-- The slideshow -->
		<div class="carousel-inner" style="width:1366px;height:620px;background-size:cover;">
			<div class="carousel-item active">
				<img src="images/s1.jpg" alt="S1">
			</div>
			<div class="carousel-item">
				<img src="images/s2.jpg" alt="Chicago" width="1366px">
			</div>
			<div class="carousel-item">
				<img src="images/s3.jpg" alt="New York">
			</div>
		</div>

		<!-- Left and right controls -->
		<a class="carousel-control-prev" href="#demo" data-slide="prev">
			<span class="carousel-control-prev-icon"></span>
		</a>
		<a class="carousel-control-next" href="#demo" data-slide="next">
			<span class="carousel-control-next-icon"></span>
		</a>

	</div>
	
	<div class="container">
		<p><h2>Profile</h2></p>
		 
		<div class="row">
		<a href='https://sia.buddhidharma.ac.id/login.php'>
         <img src='images/ubd.png' width='75px' height='75px' title='Universitas Buddhi Dharma'  />
		</a>
		<div class="col-sm-3">
				<h3><a href="https://www.instagram.com/renaldilim/?hl=id"target='blank'>Renaldi Lim</a></h3>
				<p>20161000015</p>
			</div>
			<div class="col-sm-4">
				<h3><a href="https://web.facebook.com/budi.h.kusuma.5"target='blank'>Budi Harto Kusuma</a></h3>
				<p>20161000016</p>
			</div>
			<div class="col-sm-4">
				<h3><a href="https://www.instagram.com/orlando_cand/?hl=id"target='blank'>Orlando Candra Dinata</a></h3>
				<p>20161000030</p>
				<p></p>
			</div>
		</div>
		

	</div>
</body>
</html>