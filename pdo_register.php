<?php  
session_start();  
$host = "localhost";  
$username = "root";  
$password = "";  
$database = "skripsi";  
$message = ""; 
if(isset($_SESSION["nim"]))  
{  
    header("location:index.php");  
} 
try  
{  
    $connect = new PDO("mysql:host=$host; dbname=$database", $username, $password);  
    $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
    if(isset($_POST["register"]))  
    {  
        if(empty($_POST["nim"]) || empty($_POST["password"]) || empty($_POST["nama"]))  
        {  
            $message = '<label>All fields are required</label>';  
        }  
		elseif(strlen($_POST["password"])>30 || strlen($_POST["password"])<8)
		{
			$message = '<label>Password 8-30 characters</label>';  
		}
        else  
        {  
            $statement = $connect->prepare("INSERT INTO mahasiswa(nim, password, nama) VALUES(:nim,:password,:nama)");
  
            $statement->execute(  
				array(  
					'nim'     =>     $_POST["nim"],  
					'password'     =>     md5($_POST["password"]),
					'nama'        =>     $_POST["nama"]  
				)  
            );  
            header("location:index.php");
        }  
    }  
}  
catch(PDOException $error)  
{  
    $message = $error->getMessage();  
}  
?>  
<!DOCTYPE html>  
<html>  
    <head>  
        <title>Register</title>  
		<link rel="shortcut icon" href="flat.ico">
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<script src="bootstrap/js/bootstrap.min.js"></script>
		</head>
    <body>
		<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
			<ul class="navbar-nav">
				<li class="nav-item active">
					<a class="nav-link" href="index.php">HOME</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="pdo_show.php">LIST</a>
				</li>
				<li class="nav-item">
					<?php 
					if(isset($_SESSION["nim"]))  
					{  
						echo $_SESSION["nim"];  
						echo '<a href="logout.php">Logout</a>';  
					}
					else  
					{  
						echo "<a class='nav-link' href='pdo_login.php' style='margin-left:1146px;'>LOGIN</a>";
					}  
					?>  
				</li>
			
			</ul>
		</nav>
	
        <br />
        <div class="container" style="width:500px;">
			<?php
			if(isset($message))
				{
					echo '<label class="text-danger">'.$message.'</label>';
				}
			?>
			<h3 align="center">Register</h3><br />
			<form method="post">
				<label>NIM</label>
				<input type="text" name="nim" class="form-control" />
				<br />
				<label>Password</label>
				<input type="password" name="password" class="form-control" />
				<br />
				<label>Nama</label>  
				<input type="text" name="nama" class="form-control"/>
				<br />  
				<input type="submit" name="register" class="btn btn-info" value="Register" style="width:100%; margin-top:5px;"/> 
			</form>  
		</div>  
        <br />  
    </body>  
 </html>  